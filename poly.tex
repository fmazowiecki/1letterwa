We refer to e.g.~\cite{DrosteHWA09} for an excellent introduction to weighted automata.
We consider weighted automata over the rational semiring $(\Q,+,\cdot)$, where $+$ and $\cdot$ are the standard sum and product.
For an alphabet $\Sigma$, weighted automata recognise functions assigning rational numbers to finite words,
\ie $f : \Sigma^* \to \Q$. 
In this paper we will consider only one-letter alphabets so the set of words is $\set{a}^* = \set{\varepsilon, a, a^2, \ldots}$,
which is identified with $\N$.
Therefore, weighted automata recognise functions $f : \N \to \Q$, \ie
weighted automata recognise sequences of rational numbers.

Formally, a weighted automaton is a tuple $\A = (Q, M, I, F)$, where $Q$ is a finite set of states, 
$M$ is a $Q \times Q$ matrix  over $\Q$ and $I, F$ are the initial and final vectors, respectively, of dimension $Q$ (for convenience we label the coordinates by elements of $Q$). 
The sequence recognised by the automaton $\A$ is $\sem{\A}$ defined by $\sem{\A}(n) = I^t M^n F$, 
where $I^t$ is the transpose of $I$. 

We give an equivalent definition of $\A$ in terms of accepting runs. 
We say that a state $q \in Q$ is an initial state if $I(q) \neq 0$ and that it is a final state if $F(q) \neq 0$. If $q$ is initial we say that its initial weight is $I(q)$, and if $q$ is final then its final weight is $F(q)$.
For two states $p,q \in Q$ we say that there is a transition from $p$ to $q$ if $M(p,q) \neq 0$. Such a transition is denoted $p \to q$ and its weights is $M(p,q)$.
A run $\rho$ is a sequence of consecutive transitions, and it is accepting if the first state is initial and the last state is final.
The value of an accepting run $\rho = q_0 \to q_1 \to \cdots \to q_n$ is 
\[
|\rho| = I(q_0) \cdot \left( \prod_{i = 0}^{n-1} M(q_i,q_{i+1}) \right) \cdot F(q_n).
\]
Let $\Runs$ denote the set of all accepting runs of length $n$. 
An alternative and equivalent definition of $\sem{\A}$ is 
\[
\sem{\A}(n) = \sum_{\rho \in \Runs} |\rho|.
\]

\begin{example}\label{ex:weighted}
Consider the automaton $\A = (Q, M, I, F)$ represented in Figure~\ref{fig:fib}.
We have $\sem{\A}(n) = F_n$, where $\sequence{F}$ is the Fibonacci sequence from Example~\ref{ex:fib}.
\begin{figure}[!ht]
\centering
\includegraphics[scale=.5]{Fig/fibonacci.eps}
\caption{A weighted automaton recognising the Fibonacci sequence.}
\label{fig:fib}
\end{figure}
\end{example}

\begin{example}\label{ex:weighted2}
Take a positive $k$. Consider the automaton $\B_k = (Q, M, I, F)$ represented in [TO DO FIGURE] %Figure~\ref{fig:binom}.
We have $\sem{\B_k}(n) = \dbinom{n}{k}$. Indeed, given $a^n$, there is exactly $\dbinom{n}{k}$ paths accepting this word corresponding to the choice of which letter correspond to a change of state .
%\begin{figure}[!ht]
%\centering
%\includegraphics[scale=.5]{Fig/binom.eps}
%\caption{A weighted automaton recognising the binomial coefficient.}
%\label{fig:fib}
%\end{figure}
\end{example}

The ambiguity of an automaton $\A$ is the function $a_\A : \N \to \N$ which associates to $n$ the number of accepting runs $|\Runs|$.
We consider the following classes:
\begin{itemize}
 \item $\DetWA$ -- the class of deterministic weighted automata, \textit{i.e.} such that for any $p$, there exists at most one $q$ such that $M(p,q) \neq 0$;
 \item $\kWA$ for fixed $k \in \N$ -- the class of $k$-ambiguous weighted automata, \ie when $a_\A(n) \le k$ for all $n$;
 \item $\FinWA = \bigcup_{k \in \N}\kWA$ -- the class of finitely ambiguous weighted automata, \ie when there exists $k$ such that $a_\A(n) \le k$ for all $n$;
 \item $\PolyWA$ -- class of polynomially ambiguous automata, \ie when there exists a polynomial $P : \N \to \N$ such that $a_\A(n) \le P(n)$ for all $n$;
 \item $\WA$ -- the full class of weighted automata.
\end{itemize}

For example, the automaton in Example~\ref{ex:weighted} is not polynomially ambiguous because the number of accepting runs is exponential.
We will see that this is no accident by proving in Section~\ref{sec:lrs} that the Fibonacci sequence is not in $\PolyWA$.

We present our first characterisation of $\PRat$.

\begin{theorem}\label{th:poly}
$\PRat = \PolyWA$
\end{theorem}

\subsection*{Proof of Theorem~\ref{th:poly}}
This subsection is divided into two parts for both inclusions.

\subsection*{$\PRat \subseteq \PolyWA$}

Figure~\ref{fig:arithgeo} shows how to recognise the arithmetic and the geometric sequences.
\begin{figure}[!ht]
\centering
\includegraphics[scale=.5]{Fig/arithmetic-geometric.eps}
\caption{The weighted automaton on the left recognises the arithmetic sequence with parameters $(a,b)$ and it is linearly ambiguous.
The weighted automaton on the right recognises the geometric sequence with parameters $a,\lambda$ and it is deterministic.}\label{fig:arithgeo}
\label{fig:arithmetic-geometric}
\end{figure}
For each finitely supported sequence a simple weighted automaton can be constructed.
It remains to prove that the class $\PolyWA$ is closed under the operators.
The sum and products correspond to union and product of automata,
it is readily verified that these standard constructions preserve the polynomial ambiguity. Below we deal with shift and shuffle operators.

Suppose we have a polynomially ambiguous automaton $\A$ for $\uu$ and we want to construct a new polynomially ambiguous automaton $\A'$ for $\langle a,\uu \rangle$. We start with the case when $a = 0$. Then $\A'$ has the same set of states as $\A$ plus one new state $q_0$, which is the only initial state in $\A'$. All transitions from $\A$ are inherited. There are additionally only outgoing transitions from $q_0$ to all states that are initial in $\A$; the weight of each transition is the initial weight of the corresponding state in $\A$. It is readily verified that $\A'$ recognises $\langle 0,\uu \rangle$ and that $\A'$ is polynomially ambiguous.
For $a \neq 0$ it suffices to add one more state that is both initial and final with initial weight $1$ and final weight $a$.

To deal with shuffle we start with the following preliminary construction. Fix some $k > 0$ and a polynomially ambiguous automaton $\A$ recognising $\uu$. We construct $\A[k]$ recognising $\uu' = \langle \underbrace{u_0, 0,\ldots,0}_{k}, \underbrace{u_1, 0,\ldots,0}_{k}, u_2, \ldots \rangle$, \ie elements $u_i$ are separated by $k-1$ elements with $0$. The idea to construct $\A'$ is that the set of states have an additional component $\set{0,\ldots, k-1}$, and they behave like $\A$ every $k$-th step; in the remaining steps they only wait.
Formally, the set of states of $\A[k]$ is $Q \times \set{0,\ldots,k-1}$, where $Q$ is the set of states of $\A$. The initial (final) states are $(q,0)$ such that $q$ is initial (final) in $\A$ with the same weight. For every transition $p \to q$ in $\A$ there is a transition $(p,0) \to (q,1)$ in $\A[k]$ with the same weight. The remaining transitions are $(q,i) \to (q,(i+1) \mod k)$ with weight $1$, defined for every $i > 0$ and every $q\in Q$. It is readily verified that $\A[k]$ recognises $\uu'$.

Let $\A_0,\ldots,\A_{k-1}$ be polynomially ambiguous automata recognising $\uu_0,\ldots,\uu_{k-1}$.
For every $\A_i$ let $\A_i[k]$ be an automaton as above, additionally shifted $i$ times with $0$'s. Then $\shuffleop{\uu_0,\ldots,\uu_{k-1}}$ is recognised by the disjoint union of $\A_i[k]$.

\subsection*{$\PolyWA \subseteq \PRat$}

The first step is to decompose polynomially ambiguous automata into a union of automata that we will call \emph{chained loops}.
We say that the states $p_0, p_1, \ldots p_{k-1} \in Q$ form a \emph{loop} if $M(p_i, p_j) \neq 0$ for $j = i+1 \mod k$
and a \emph{path} if $M(p_i, p_j) \neq 0$ for $j = i+1$ (in particular $p_{k-1}$ has no successor). We say a loop $p_0, p_1, \ldots p_{k-1}$ is a \emph{simple loop} if the states do not repeat, \ie if $p_i = p_j$ then either $i = j$ or $\{i,j\} =\{0,k-1\}$. A \emph{simple path} is defined accordingly.
A chained loop of size $k$ is an automaton over the set states of $\set{q_0, \ldots, q_{k-1}} \cup P$ such that
\begin{itemize}
	\item $q_0$ is the unique initial state;
	\item $q_0, \ldots, q_{k-1}$ form a simple path;
	\item each $q_i$ is contained in at most one simple loop (the states in $P$ are used only as intermediate states in the loops);
	\item $q_{k-1}$ is the unique final state with $F(q_{k-1}) = 1$.
\end{itemize}
We define the concatenation of two chained loops $\A_1, \A_2$: this is the chained loop
obtained by constructing the union of the two automata
with the initial state being the initial state of $\A_1$, the final state being the final state of $\A_2$, and
rewiring the output of $\A_1$ to the initial state of $\A_2$, see \eg Figure~\ref{fig:chained}.

\begin{figure}[!ht]
\centering
\includegraphics[scale=.4]{Fig/examples.eps}
\caption{Three example chained loops. The initial and final weights are depicted by ingoing and outgoing edges. The chained loop $\A_1$ recognises the sequence defined by $f_1(2n) = 2 \cdot 3^n$, $f_1(2n+1) = 0$ whose power series is $\frac{2}{1 - 3x^2}$. The chained loop $\A_2$ recognises the sequence $f_2(n) = 5^{n+1}$ whose power series is $\frac{5}{1-5x}$. The chained loop $\A_3$ is the concatenation of $\A_1$ and $\A_2$ and it recognises the sequence $f_3(n) = \sum_{i=1}^n f_1(i-1) \cdot f_2(n-i)$ whose power series is $\frac{10x}{(1-3x^2)(1-5x)}$.}\label{fig:chained}
\end{figure}

\begin{lemma}\label{lem:poly-chained}
Every polynomially ambiguous weighted automaton is equivalent to a union of chained loops.
\end{lemma}

\begin{proof}
Let $\A$ be a polynomially ambiguous weighted automaton. 
Without loss of generality $\A$ is trimmed, \ie every state occurs in at least one accepting run.

We first note that any state in $\A$ is contained in at most one simple loop.
Indeed, a state contained in two simple loops induces a sequence of words with exponential ambiguity.
This implies that a sequence $(q_0,q_1,\dots,q_k)$ with $q_i \neq q_j$ for $i \neq j$ induces at most one chained loop of which it is the path.
There are finitely many such sequences because $k$ is bounded by the number of states of $\A$.

We claim that $\A$ is equivalent to the union of all chained loops induced by such sequences.
Indeed, there is a bijection between the runs of $\A$ and the runs of all the chained loops, respecting the values of runs.
Consider a run $\rho$ of $\A$, where a state $q$ appears multiple times. 
Then between each occurrence of $q$ this is the same run, because they are loops over $q$ and there can be only one simple loop containing $q$.
So $\rho = u v^k w$, where $v$ is the unique simple loop containing $q$.
Repeating this for $u$ and $w$, we obtain a unique decomposition of $\rho$ into 
\[
q_0 \cdot \ell_0^{m_0} \cdot q_0 \to q_1 \cdot \ell_1^{m_1} \cdot q_1 \to \dots \to q_k \cdot \ell_n^{m_k} \cdot q_k,
\]
where $\ell_i$ is a loop over $q_i$ (we can have $m_i = 0$) and $q_i \neq q_j$ for $i \neq j$.
\end{proof}

Our aim is to use the decomposition result stated in Lemma~\ref{lem:poly-chained} to prove the inclusion 
$\PolyWA \subseteq \PRat$.
It will be convenient for reasoning to use formal series.

\begin{lemma}\label{lem:chained}\hfill
\begin{itemize}
	\item The formal series induced by a chained loop of size $1$ with a loop is of the form $\frac{\alpha}{1 - \lambda x^{\ell}}$,
where $\alpha = I(q_0)$, $\lambda$ is the product of the weights in the loop and $\ell$ is the length of the loop. 
If there is no loop this reduces to $\alpha$.
	\item Let $S_1,S_2$ be the formal series induced by the chained loops $\A_1$ and $\A_2$,
	then the formal series induced by the concatenation of $\A_1$ and $\A_2$ is $x \cdot S_1 \cdot S_2$. 
	\item Let $S_1,S_2$ be the formal series induced by two automata $\A_1$ and $\A_2$,
	then the formal series induced by the union of $\A_1$ and $\A_2$ is $S_1 + S_2$.
\end{itemize}
\end{lemma}

\begin{proof}
The first and the third item are immediate, we focus on the second. %For convenience let us assume that $\A_1(-1) = 0$.
By definition the concatenation of two chained loops recognises the sequence defined by 
\[
\sem{\A}(n) = \sum_{i = 1}^n \sem{\A_1}(i-1) \cdot \sem{\A_2}(n-i) 
= \sum_{i = 0}^{n-1} \sem{\A_1}(i) \cdot \sem{\A_2}(n-i-1),
\]
since an accepting run in the concatenation is the concatenation of an accepting run in $\A_1$ and an accepting run in $\A_2$. The only issue is that the output state of $\A_1$ was changed into a transition, and to include this step we write $\A_1(i-1)$ instead of $\A_1(i)$.
Hence the formal series is indeed the Cauchy product of $S_1$ and $S_2$, shifted by one.
\end{proof}

We are now half-way through the proof of the inclusion $\PolyWA \subseteq \PRat$:
thanks to Lemma~\ref{lem:poly-chained}, we can restrict our attention to unions of chained loops,
and thanks to Lemma~\ref{lem:chained}, we know what are the formal series induced by the sequences computed by such automata.
More specifically, they are obtained from formal series of the form $\frac{\alpha}{1-\lambda x^\ell}$
by taking sums and Cauchy products (with an additional shift).

To prove that $\PRat$ contains such sequences it is tempting to attempt showing that the sequences above are in $\PRat$
and the closure of $\PRat$ under sums and Cauchy products.
Unfortunately, the closure under Cauchy product is not clear (although it will follow from the final result that it indeed holds).

We sidestep this issue by observing that we only need to be able to do Cauchy products of formal series of a special form.
Indeed, the formal series described above are of the form $\frac{P}{Q}$ where $P,Q$ are rational polynomials 
and the roots of $Q$ are roots of rational numbers: this is true of $\frac{\alpha}{1-\lambda x^\ell}$ and is clearly closed under sums and Cauchy products (with the additional shift).

Notice that every chained loop can be obtained as concatenations of chained loops of size~$1$. Thus Lemma~\ref{lem:chained} gives a characterisation of formal series corresponding to unions of chained loops: these are sums of products of $\frac{\alpha}{1 - \lambda x^{\ell}}$ and polynomials.
We further simplify this characterisation applying the following lemma.

\begin{lemma}\label{lem:euclidian}
Consider the formal series $\frac{P}{Q}$ where $P,Q$ are rational polynomials and the roots of $Q$ are roots of rational numbers.
Then $\frac{P}{Q}$ can be written as the sum of formal series of the form $\frac{R}{(1 - \lambda x^\ell)^k}$ 
for rational polynomials $R$, rational numbers $\lambda$, and $\ell,k$ natural numbers.
\end{lemma}

\begin{proof}
This is a direct consequence of the fact that $\Q[x]$ is a Euclidean ring.
The exact statement following from this is that 
any product $\prod_{i = 1}^n \frac{R_i}{P_i}$ where the polynomials $P_i$ are mutually prime 
(meaning, for each $i$, the polynomials $P_i$ and $\prod_{j \neq i} P_j$ are coprime)
can be written as a sum of $\frac{Q_i}{P_i}$ for some rational polynomials $Q_i$.

To conclude, we observe that any polynomial whose roots are roots of rational numbers
can be written as a product of mutually prime polynomials of the form $(1 - \lambda x^\ell)^k$.
\end{proof}

By Lemma~\ref{lem:chained} and Lemma~\ref{lem:euclidian} it follows that for every finite union of chained loops its formal series is a sum of $\frac{R}{(1 - \lambda x^\ell)^k}$ for rational polynomials $R$, rational numbers $\lambda$, and $\ell,k$ natural numbers.
Combining this with Lemma~\ref{lem:poly-chained}
we get that the formal series computed by $\PolyWA$ are of the same form.
Thus we have reduced proving the inclusion $\PolyWA \subseteq \PRat$ to
proving that sequences whose formal series are sums of formal series of the form $\frac{R}{(1 - \lambda x^\ell)^k}$ are in $\PRat$. 

Since $\PRat$ is closed under sum, it suffices to consider one such formal series.
Moreover, due to the closure under shifts we can assume that the polynomial $R$ is equal to $1$; as stated in the lemma below.

\begin{lemma}\label{lem:rationa-roots}
The sequence whose formal series is $\frac{1}{(1 - \lambda x^\ell)^k}$ is in $\PRat$.
\end{lemma}

\begin{proof}
We know that
\[
\frac{1}{(1 - \lambda x^\ell)^k} = \sum_{n \in \N} \binom{n + k - 1}{k-1} \lambda^n x^{\ell \cdot n}.
\]
Note that $\binom{n + k - 1}{k-1}$ is a polynomial in $n$ of degree at most $k-1$, 
i.e. $\binom{n + k - 1}{k-1} = \sum_{p = 0}^{k-1} a_p n^p$.
It follows that
\[
\frac{1}{(1 - \lambda x^\ell)^k} = \sum_{p = 0}^{k-1}\ a_p \cdot \sum_{n \in \N} n^p \lambda^n x^{\ell \cdot n}
\]
It is enough to prove that for each $p$ the sequence whose formal series is 
\[
\sum_{n \in \N} a_p n^p \lambda^n x^{\ell \cdot n}
\]
is in $\PRat$. 
Using an arithmetic sequence and Hadamard products we construct $\langle a_p n^p \rangle_{n \in \N}$.
Multiplying it using Hadamard product with the geometric sequence $\langle \lambda^n \rangle_{n \in \N}$
yields $\langle a_p n^p \lambda^n \rangle_{n \in \N}$.
Shuffling the obtained sequence with $\ell - 1$ null sequences yields the desired sequence.
\end{proof}

\subsection{Application: the ambiguity hierarchy of weighted automata}
We show that the natural classes of weighted automata defined by ambiguity can be described using subclasses of rational expressions.

\begin{figure}[!ht]
\centering
\includegraphics[scale=.6]{Fig/hierarchy.eps}
\caption{The strict ambiguous hierarchy of weighted automata.}
\label{fig:hierarchy}
\end{figure}

\begin{lemma}\label{lem:wa-classes}\hfill
\begin{itemize}
	\item $\DetWA = \bigcup_{\lambda \in \Q} \shuffle\left(\Rat[\Geo_\lambda, \shift]\right)$; ($\shuffle$ can be used only once)
 	\item $\kWA = \sum_{i=1}^k\Rat[\Geo, \shift, \shuffle]$, (\ie $+$ is used at most $k$ times);
	\item $\FinWA = \Rat[\Geo, +, \shift, \shuffle]$.
\end{itemize}
\end{lemma}

\begin{proof}
We start by proving $\DetWA = \bigcup_{\lambda \in \Q} \Rat[\Geo_\lambda, \shift, \shuffle]$.

($\subseteq$) Since the automaton is deterministic it has a shape of a lasso, \ie the states can be partitioned into a path such that the last state on the path is in a loop. Let $\lambda$ be the value obtained by multiplying all values on the loop, let $l$ be the length of the loop and let $m$ be the length of the path. Then it is easy to see that the sequence is obtained by first taking a shuffle of $l$ sequences in $\Geo_\lambda$ that are shifted accordingly.

($\supseteq$) We already know that $\Geo_\lambda$ are definable by deterministic weighted automata from Figure~\ref{fig:arithmetic-geometric}. Closure under shift follows from the construction in the proof of $\PRat \subseteq \PolyWA$ because it preserves the property of being deterministic. 
%
To prove the general case suppose we have a sequence definable by $\shuffle(u_1,...,u_k)$ where every sequence $u_i$ is defined as shifts of geometric sequences with a common $\lambda$. Then by generalising the automaton in Figure~\ref{fig:arithmetic-geometric} for every $u_i$
there is a deterministic automaton $\A_i$ with a loop of length 1 whose value is $\lambda$. The final automaton is a lasso constructed as follows.
\begin{itemize}
 \item The nonloop part will be of length $km$, where $m$ is the maximal number of states among all $\A_i$. The weights are defined in such a way that the values for the first $km$ elements are correct.
 \item The loop will be of length $k$ defined in such a way that all values of the first $km+k$ elements are correct. Then the last transition that closes the loop is defined in such a way that the product on the loop is exactly $\lambda$.
\end{itemize}
It is easy to verify that the construction works because all $u_i$ where defined from geometric sequences with the same $\lambda$.

Proof of $\FinWA = \Rat[\Geo, +, \shift, \shuffle]$.

($\subseteq$) By Lemma~\ref{lem:poly-chained} we know that each automaton in $\FinWA$ is a union of chained loops. It is easy to see that every such chained loop has to be a lasso otherwise it will contradict the assumption that the automaton is finitely ambiguous. Then the construction follows by doing the construction for every lasso as in the proof of $\DetWA = \bigcup_{\lambda \in \Q} \Rat[\Geo_\lambda, \shift, \shuffle]$ and using $+$ to deal with the union.

($\supseteq$)
Like in the proof of $\DetWA = \bigcup_{\lambda \in \Q} \Rat[\Geo_\lambda, \shift, \shuffle]$ all steps except for $\shuffle$ are easy. Suppose we want to define a sequence $\shuffle(u_1,...,u_k)$, where every $u_i$ is definable by an automaton $\A_i \in \FinWA$.
Recall the construction of automata $\A_i[k]$ in the proof of closure under shuffle in $\PRat \subseteq \PolyWA$.
It is readily verified that this construction keeps the property of being finitely ambiguous.

Proof of $\kWA = \sum_{i=1}^k\Rat[\Geo, \shift, \shuffle]$.

($\subseteq$) It suffices to apply the proof for $\FinWA = \Rat[\Geo, +, \shift, \shuffle]$ with a bit more work. We first write the finitely ambiguous automaton as a union of lassos, with the additional condition that all loops have the same length. Now because the ambiguity is bounded by $k$, for each position is the loop, there are at most $k$ lassos that are accepting. Hence only $k$ sum operations are required.
\filip{This is nonsense and I think the lemma might be not true. What you could conclude from this is that if you divide the sequence into subsequences then each subsequence requires only $k$ sums. But then you need to take the sum of all subsequences. If you do the lasso construction then it is not hard to find an example that for unambiguous automata you might need to use sum. I would remove this.}

($\supseteq$) This follows the same steps as the proof of $\FinWA = \Rat[\Geo, +, \shift, \shuffle]$. This time, we have to remark that the union of $k$ deterministic automata is at most $k-$ambiguous.
\end{proof}

We give examples witnessing the strict inclusions $\DetWA \subsetneq \FinWA \subsetneq \PolyWA \subsetneq \WA$
and $\kWA \subsetneq \kplusoneWA$.
\begin{lemma}\label{lem:hierarchy}\hfill
\begin{itemize}
	\item $\aa = \shuffleop{\langle 2^n \rangle_{n \in \N}, \langle 1 \rangle_{n \in \N}}$ is in $\oneWA$ but not in $\DetWA$,
	\item $\uu^{(k)}$ defined by $u^{(k)}_n = 1^n + 2^n + \cdots + (k+1)^n$ is in $\kplusoneWA$ but not in $\kWA$,
	\item $\vv$ defined by $v_n = n$ is in $\PolyWA$ but not in $\FinWA$;
	\item Fibonacci is in $\WA$ but not in $\PolyWA$.
\end{itemize}
\end{lemma}

We will give here the proofs of the first three items.
Only the last item will be proved in Section~\ref{sec:lrs}, it follows from the fact that $\PolyWA = \PRat$ 
is equal to the class of LRS whose eigenvalues are roots of rational numbers. 
As mentioned in Example~\ref{ex:fib} the characteristic polynomial of the Fibonacci sequence is
$x^2-x-1$, so its eigenvalues are not roots of rationals.

\begin{figure}[ht]
\begin{subfigure}{.5\textwidth}
  \centering
  % include first image
  \includegraphics[width=.8\linewidth]{Fig/example1}  
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  % include second image
  \includegraphics[width=.8\linewidth]{Fig/example2}  
\end{subfigure}
\caption{Separating examples for the ambiguity hierarchy.}
\label{fig:examples}
\end{figure}

\begin{proof}
 \begin{itemize}
  \item The sequence $\aa$ is recognised by the $1$-ambiguous weighted automaton represented on the left hand side of Figure~\ref{fig:examples}. 
  Now, assume that $\aa$ can be recognized by some deterministic weigthed automaton. By the previous characterisation, there 
  exists a rationnal $\lambda$ such that $\aa\in\Rat[\Geo_\lambda, \shift, \shuffle]$. Let $n_0$ be the constant of the shift and $p$ be the number of shuffled sequences that are equals to 
  $\aa$. Without loss of generality, we can assume that $p$ and $n_0$ are even. With those parameters, we have that $\aa_{n_0 + p} = \lambda \aa_{n_0}$ and $\aa_{n_0 + p+1} = \lambda \aa_{n_0+1}$. But 
 we know that $\aa_{n_0} = 2^{n_0/2}$ and $\aa_{n_0 + p} = 2^{(n_0+p)/2}$ and $\aa_{n_0+1} = \aa_{n_0+p+1} = 1$. This is a contradiction because $\lambda$ is equal to two distincts values.
 
  \item The sequence $\uu^{(k)}$ is recognised by the $(k+1)$-ambiguous weighted automaton represented on the right hand side of Figure~\ref{fig:examples}.
For the other part,
  assume $\uu^{(k)}$ is in $\kWA$, which is the same as $\sum_{i=1}^k\Rat[\Geo, \shift, \shuffle]$. This means that we can find a shift $n_0$ and a period $p$ such that 
  $\uu^{(k)}_{n_0+pn} = a_1 \lambda_1^n + \cdots + a_k \lambda_k^n$ for some constants $\lambda_1 < \cdots < \lambda_k$. So for every $n$, 
  $1^{n_0+pn} + \cdots + (k+1)^{n_0+pn} = a_1 \lambda_1^n + \cdots + a_k \lambda_k^n$. By taking asymptotic equivalent in both sides, we obtain $a_k = (k+1)^{n_0}$ and $\lambda_k = (k+1)^p$,
  therefore $a_k \lambda_k^n = (k+1)^{n_0+pn}$. We can thus simplify both sides of the equation and repeat recursively the process, leading to $ 1 =0$, which is a contradiction.
  
  \item First, $\vv$ is an arithmetic sequence so it is in $\PolyWA$. Now assume it is in $\FinWA$. For the same reasons as before, there are a shift $n_0$, a period $p$ and constants 
  $(\lambda_i)_{1\leq i \leq m}$ such that $n_0 +pn = \lambda_1^n + \cdots + \lambda_m^n$. A quick asymptotic argument shows that those two sequences cannot be equal. 
 
  
 \end{itemize}

\end{proof}

