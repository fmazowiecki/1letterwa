We let $\uu = \sequence{u} = \langle u_0,u_1,u_2\ldots \rangle$ denote a sequence of rational numbers.
%, or equivalently a function $f : \N \to \Q$.

\paragraph*{Linear recurrence sequences}
We will assume that an LRS $\uu$ is given by the rational numbers $a_1, \ldots, a_k$ and the rational values of the first $k$ elements: 
$u_0,\ldots, u_{k-1}$. The recurrence~\eqref{eq:lrs} induces the sequence $\uu$.
We let $\LRS$ denote the class of LRS.
Given an LRS we define its characteristic polynomial as
\begin{align*}
Q(x) \; = \; x^k - a_{1}x^{k-1} - \ldots - a_{k-1}x - a_k.
\end{align*}
The roots of the characteristic polynomial are called the \emph{eigenvalues} of the LRS.

\paragraph*{Formal series}
Formal series are a different representation for sequences.
The sequence $\sequence{u}$ induces the formal series $S(x) = \sum_{n \in \N} u_n x^n$, 
with the interpretation that the coefficient of $x^n$ is the value of the $n$-th element in the sequence.
Note that a polynomial represents a sequence with a finite support. 

\begin{example}\label{ex:fib}
A standard example of an LRS is the Fibonacci sequence $\sequence{F}$ defined by the recurrence $F_{n+2} = F_{n+1} + F_n$ and initial values $F_0 = 0, F_1 = 1$. 
Its characteristic polynomial is $p(x) = x^2 - x - 1$, whose roots are $\frac{1+\sqrt{5}}{2}$ and $\frac{1 - \sqrt{5}}{2}$. 
The corresponding formal series is $S(x) = \sum_{n=0}^\infty F_nx^n$. 
Using the definition of $F$ we obtain $S(x) = x + xS(x) + x^2S(x)$ and thus $S(x) = \frac{x}{1 - x - x^2}$.
\end{example}

\paragraph*{Rational expressions}
%For a rational number $a$ we let $a$ denote the sequence $(a,0,0,\ldots)$.
%This way we can see $\Q$ as a class of sequences.
We start by defining three classes of sequences.
\begin{itemize}
	\item $\Fin$: a sequence $\uu$ is in $\Fin$, or equivalently $\uu$ has finite support, if the set $\set{n \in \N : u_n \neq 0}$ is finite;
	\item $\Arith$: a sequence $\uu$ is in $\Arith$, or equivalently $\uu$ is arithmetic, if $u_0 = a$, $u_{n+1} = u_n + b$ for some rational numbers $a,b$;
	\item $\Geo$: a sequence $\uu$ is in $\Geo$, or equivalent $\uu$ is geometric, if $u_0 = a$, $u_{n+1} = \lambda \cdot u_n$, for some rational numbers $a,\lambda$.
\end{itemize}
We let $\Geo_\lambda$ denote the class of geometric sequences with a fixed parameter $\lambda$.

\vskip1em
We now define some classical operators. Here $\uu, \vv, \uu^1, \ldots, \uu^k$ are sequences.
\begin{itemize}
	\item \textbf{Sum}: $\uu + \vv$ is the component wise sum of sequences;
	\item \textbf{Cauchy product}: $\uu \cdot \vv = \langle \sum_{p+q=n} u_p \cdot v_q \rangle_{n \in \N}$;
	inducing $(\uu)^n$ defined by $(\uu)^0 = \langle 1,0,0,0,\ldots \rangle$ and $(\uu)^{n+1} = (\uu)^n \cdot \uu$, in particular $(\uu)^1 = \uu$;
	\item \textbf{Kleene star}: $\left(\uu \right)^* = \sum_{n \in \N} \left( \uu \right)^n$, it is only defined when $u_0 = 0$;
%	\item For $\uu$ such that $u_0 = 0$, $\uu^* = \langle \sum_{p_1 + \ldots + p_k = n} \prod_i u_{p_i} \rangle_{n\in \N}$ is called the Kleene star of the sequence;
	\item \textbf{Hadamard product}: $\uu \times \vv$ is the component wise product of sequences;
	\item \textbf{Shift}: $\langle a,\uu \rangle = \langle a,u_0,u_1,\ldots \rangle$, defined for any rational number $a$;
	\item \textbf{Shuffle}: $\shuffleop{\uu^1,\uu^2,\ldots,\uu^k} = \langle u^1_0,u^2_0,\ldots,u^k_0,u^1_1,u^2_1,\ldots,u^k_1,u^1_2,\ldots \rangle$.
\end{itemize}

%We let $\ArithGeo$ denote the class of arithmetic-geometric sequences.
%Let $\Geo$ denote the class of geometric sequences, \textit{i.e.} arithmetic-geometric sequences with $b = 0$,
%and $\Geo_\lambda$ when fixing the parameter $\lambda$.
We write $\Rat[\C,\text{op}_1,\ldots,\text{op}_k]$ for the smallest class of sequences containing $\C$ and closed under the operators
$\text{op}_1,\ldots,\text{op}_k$.
Rational expressions in Theorem~\ref{th:classical} are classically defined as follows~\cite{Schutzenberger61b}:
\[
\Rat = \Rat[\Fin,+,\cdot,*].
\]
The class $\Rat$ contains all classes defined above, and is closed under all mentioned operators, \ie 
\[
\Rat = \Rat[\Fin \cup \Arith \cup \Geo,+,\cdot,*,\times,\shift,\shuffle].
\]

% \vskip1em
We now introduce a class of sequences denoted by a fragment of rational expressions,
whose study is the purpose of this article.
The class is called poly-rational sequences, because they are denoted by rational expressions using sum and product.

\begin{definition}[Poly-rational sequences]
\[
\PRat = \Rat[\Arith \cup \Geo, +, \times, \shift, \shuffle].
\]
\end{definition}
In other words $\PRat$ is the smallest class of sequences containing arithmetic and geometric sequences that is
closed under sum, Hadamard product, shift, and shuffle. A trivial observation is that $\Fin \subseteq \PRat$ since using $\shift$ one can generate any sequence with finite support. One could try to simplify the definition of $\PRat$ replacing $\Arith \cup \Geo$ with $\Fin$. Unfortunately, the operators $+, \times, \shift, \shuffle$ are too restricted, and geometric and arithmetic sequences could not be generated. In fact, the class would collapse to $\Fin$. 

Since $\Rat$ contains $\Arith$ and $\Geo$ and is closed under Hadamard product, shift, and shuffle,
we have $\PRat \subseteq \Rat$. We will show that the inclusion is indeed strict.
As we will see in this paper, the class $\PRat$ has many equivalent and surprising characterisations.
% Moreover one of the consequences of our results is that $\PRat$ is closed under the Cauchy product\filip{maybe a reference to future corollary?}.
