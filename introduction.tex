The study of sequences of numbers originated in mathematics and has deep connections with many fields.
A prominent class of sequences is that of \emph{linear recurrence sequences}, such as the Fibonacci sequence
\[
0,1,1,2,3,5,8,13,\ldots
\]
Despite the simplicity of linear recurrence sequences many problems related to them remain open, 
and are the object of active research. 
In theoretical computer science the two main questions are:
\begin{itemize}
	\item How to finitely represent sequences?
	\item How to algorithmically analyse properties of sequences?
\end{itemize}

In this paper we focus on problems related to the first question. 
The question of representation has led to important insights in the structure of linear recurrence sequences
by giving several equivalent characterisations, some of which we briefly review here.
We refer to Section~\ref{sec:rat} and the next sections for technical definitions.

\subparagraph*{Linear recurrence sequences}
A sequence of rational numbers $\uu = \sequence{u} = \langle u_0, u_1, u_2, \ldots \rangle$ is a linear recurrence system (LRS) if there exist real numbers $a_1, \ldots, a_k$ such that %$a_k \neq 0$ and 
for all $n \ge 0$
\begin{align}\label{eq:lrs}
u_{n+k} \; = \; a_1 u_{n+k-1} + \ldots + a_k u_{n}.
\end{align}
In this paper we will consider only sequences of rational numbers, therefore, we additionally assume that $a_i$ are rational numbers. 
The smallest $k$ for which $\uu$ satisfies an equation of the form~\eqref{eq:lrs} is called the order of $\uu$. 
The Fibonacci sequence $\sequence{F}$ is an LRS of order~$2$ satisfying the recurrence $F_{n+2} = F_{n+1} + F_n$.

\subparagraph*{Rational expressions}
Studying the closure properties of linear recurrence sequences yields the following result,
an instance of the Kleene-Sch\"{u}tzenberger theorem~\cite{Schutzenberger61b}:
linear recurrence sequences form the smallest class of sequences containing the sequences $\langle a,0,0,\ldots \rangle$ for a rational number $a$
and closed under sum, Cauchy product, and Kleene star.

\subparagraph*{Weighted automata}
The model of weighted automata is a well studied quantitative extension of classical automata.
In general a weighted automaton recognises a function $f : \Sigma^* \to \R$,
hence when considering a unary alphabet this becomes $f : \set{a}^* \to \R$, 
and identifying $\set{a}^*$ with $\N$ we can see $f$ as a sequence of numbers.
Whenever we write about sequences recognised by models like weighted automata, we implicitly assume that these are over a unary alphabet.

\subparagraph*{Cost-register automata}
Several characterisations of weighted automata have been introduced~\cite{DrosteG07,KreutzerR13,AlurDDRY13}.
We will be interested in the model of \emph{cost-register automata} (CRA). 
These are deterministic models with registers whose contents are blindly updated (i.e., without transitions like zero tests). 
It was shown that considering linear updates yields a model equivalent to weighted automata.

\vskip1em
We summarise in one theorem the equivalences above, which is the starting point of our work.
Technical definitions are given in the paper.
\begin{theorem}[Folklore, see for instance~\cite{Bousquet-Melou05,Schutzenberger61b,DrosteHWA09}]\label{th:classical}
The following classes of sequences are effectively equivalent.
\begin{itemize}
	\item Linear recurrence sequences,
	\item Sequences recognised by weighted automata,
	\item Sequences recognised by linear cost-register automata,
	\item Sequences denoted by rational expressions,
	\item Sequences whose formal series are rational, \textit{i.e.} of the form $\frac{P}{Q}$ where $P,Q$ are polynomials.
\end{itemize}
\end{theorem}

\paragraph*{Algorithmic analysis of linear recurrence sequences}
The questions regarding algorithmic analysis are far from being answered. 
A very simple and natural problem, the Skolem problem, is still unsolved~\cite{tao2008structure,OuaknineW15}:
given a linear recurrence sequence, does it contain a zero?
Recent breakthrough results sharpened our understanding of the Skolem problem~\cite{OuaknineW14,OuaknineW14a},
but one of the outcomes is that the general problem for the whole class of linear recurrence sequences is beyond our reach at the moment,
since it would impact notoriously difficult problems from number theory.
We refer the reader to the recent survey about what is known to be decidable for linear recurrence sequences~\cite{OuaknineW15}. 

\paragraph*{Our contributions}
Since the full class of linear recurrence sequences is too hard to be algorithmically analysed (we only mentioned the Skolem problem but many related problems are also difficult), let us revise our ambitions, go back to the drawing board, and study tractable subclasses.

In this paper we introduce \emph{poly-rational sequences} which is a strict fragment of linear recurrence sequences. 
We give several equivalent characterisations of this class following the equivalence results stated in Theorem~\ref{th:classical}.
Our results are summarised in the following theorem.
\begin{theorem}\label{th:main}
The following classes of sequences are effectively equivalent.
\begin{itemize}
	\item Sequences denoted by poly-rational expressions (Section~\ref{sec:rat}),
	\item Sequences recognised by polynomially ambiguous weighted automata (Section~\ref{sec:poly}),
	\item Sequences recognised by copyless cost-register automata (Section~\ref{sec:ccra}),
	\item Sequences whose formal series are of the form $\frac{P}{Q}$ where $P,Q$ are polynomials 
	and the roots of $Q$ are roots of rational numbers (Section~\ref{sec:lrs}),
	\item Linear recurrence sequences whose eigenvalues are roots of rational numbers (Section~\ref{sec:lrs}).
\end{itemize}
\end{theorem}

We do not discuss the efficiency of reductions proving the equivalences. 
Our constructions are elementary, and in most cases they yield blow ups in the size of representation.

We note that the Skolem problem and its variants are known to be decidable, and NP-hard, for the subclass of poly-rational sequences.
The decidability easily follows from the fact that our class is subsumed by other classes for which such results were obtained (see e.g.~\cite{RebihaMM14b}, for the case where all eigenvalues are roots of algebraic real numbers).
The Skolem problem is known to be NP-hard already for the class of LRS whose eigenvalues are roots of unity~\cite{ABV17}. 
This implies that the Skolem problem for the class of poly-rational sequences is also NP-hard, 
which is the best known lower bound even for the full class of linear recurrence sequences.
Recently it was shown that the Skolem problem for poly-rational sequences is in NP$^{\text{RP}}$, which gives a near optimal complexity bound~\cite{AkshayBMV020}.

\paragraph*{Related works}
The intractability of the Skolem problem for linear recurrence sequences also impacts the other equivalent models,
leading to the study of several restrictions.
A classical approach to tame weighted automata is to bound the ambiguity of weighted automata, \ie 
bounding the number of accepting runs with a function depending on the length of the word. 
Many positive results have been obtained in the past years following this approach~\cite{KlimannLMP04,KirstenL09,FijalkowRW17}.

Another restriction studied in the model of cost-register automata is the \emph{copyless} restriction: 
registers are not allowed to be copied more than once.
It was conjectured that the copyless restriction would result in good decidability properties~\cite{AlurDDRY13}, 
but this has been recently falsified~\cite{AlmagorCMP18}.
