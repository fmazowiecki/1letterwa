We let $\uu = \sequence{u} = \langle u_0,u_1,u_2\ldots \rangle$ denote a sequence of rational numbers.
%, or equivalently a function $f : \N \to \Q$.

\paragraph*{Linear recurrence sequences}
We will assume that an LRS $\uu$ is given by the rational numbers $a_1, \ldots, a_k$ and the rational values of the first $k$ elements: 
$u_0,\ldots, u_{k-1}$. The recurrence~\eqref{eq:lrs} induces the sequence $\uu$.
We let $\LRS$ denote the class of LRS.
Given an LRS we define its characteristic polynomial as
\begin{align*}
Q(x) \; = \; x^k - a_{1}x^{k-1} - \ldots - a_{k-1}x - a_k.
\end{align*}
The roots of the characteristic polynomial are called the \emph{eigenvalues} of the LRS.

\paragraph*{Rational expressions}
%For a rational number $a$ we let $a$ denote the sequence $(a,0,0,\ldots)$.
%This way we can see $\Q$ as a class of sequences.
We start by defining three classes of sequences.
\begin{itemize}
	\item $\Fin$: a sequence $\uu$ is in $\Fin$, or equivalently $\uu$ has finite support, if the set $\set{n \in \N : u_n \neq 0}$ is finite;
	\item $\Arith$: a sequence $\uu$ is in $\Arith$, or equivalently $\uu$ is arithmetic, if $u_0 = a$, $u_{n+1} = u_n + b$ for some rational numbers $a,b$;
	\item $\Geo$: a sequence $\uu$ is in $\Geo$, or equivalent $\uu$ is geometric, if $u_0 = a$, $u_{n+1} = \lambda \cdot u_n$, for some rational numbers $a,\lambda$.
\end{itemize}
We let $\Geo_\lambda$ denote the class of geometric sequences with a fixed parameter $\lambda$.

\vskip1em
We now define some classical operators. Here $\uu, \vv, \uu^1, \ldots, \uu^k$ are sequences.
\begin{itemize}
	\item \textbf{Sum}: $\uu + \vv$ is the component wise sum of sequences;
	\item \textbf{Cauchy product}: $\uu \cdot \vv = \langle \sum_{p+q=n} u_p \cdot v_q \rangle_{n \in \N}$;
	inducing $(\uu)^n$ defined by $(\uu)^0 = \langle 1,0,0,0,\ldots \rangle$ and $(\uu)^{n+1} = (\uu)^n \cdot \uu$, in particular $(\uu)^1 = \uu$;
	\item \textbf{Kleene star}: $\left(\uu \right)^* = \sum_{n \in \N} \left( \uu \right)^n$, it is only defined when $u_0 = 0$ as in this case $(\uu)^n$ begins with $n$ zeros, making the sum finite for each component;
%	\item For $\uu$ such that $u_0 = 0$, $\uu^* = \langle \sum_{p_1 + \ldots + p_k = n} \prod_i u_{p_i} \rangle_{n\in \N}$ is called the Kleene star of the sequence;
	\item \textbf{Hadamard product}: $\uu \times \vv$ is the component wise product of sequences;
	\item \textbf{Shift}: $\langle a,\uu \rangle = \langle a,u_0,u_1,\ldots \rangle$, defined for any rational number $a$;
	\item \textbf{Shuffle}: $\shuffleop{\uu^1,\uu^2,\ldots,\uu^k} = \langle u^1_0,u^2_0,\ldots,u^k_0,u^1_1,u^2_1,\ldots,u^k_1,u^1_2,\ldots \rangle$.
\end{itemize}

%We let $\ArithGeo$ denote the class of arithmetic-geometric sequences.
%Let $\Geo$ denote the class of geometric sequences, \textit{i.e.} arithmetic-geometric sequences with $b = 0$,
%and $\Geo_\lambda$ when fixing the parameter $\lambda$.
We write $\Cl[\C,\text{op}_1,\ldots,\text{op}_k]$ for the smallest class of sequences containing $\C$ and closed under the operators
$\text{op}_1,\ldots,\text{op}_k$.
Rational expressions in Theorem~\ref{th:classical} are classically defined as follows~\cite{Schutzenberger61b}:
\[
\Rat = \Cl[\Fin,+,\cdot,*].
\]
The class $\Rat$ contains all classes defined above, and is closed under all mentioned operators (see for instance~\cite{DBLP:books/daglib/0023547}), \ie 
\[
\Rat = \Cl[\Fin \cup \Arith \cup \Geo,+,\cdot,*,\times,\shift,\shuffle].
\]

% \vskip1em
We now introduce a class of sequences denoted by a fragment of rational expressions,
whose study is the purpose of this article.
The class is called poly-rational sequences, because they are denoted by rational expressions using sum and product.

\begin{definition}[Poly-rational sequences]
\[
\PRat = \Cl[\Arith \cup \Geo, +, \times, \shift, \shuffle].
\]
\end{definition}
In other words $\PRat$ is the smallest class of sequences containing arithmetic and geometric sequences that is
closed under sum, Hadamard product, shift, and shuffle. A trivial observation is that $\Fin \subseteq \PRat$ since using $\shift$ and $+$ one can generate any sequence with finite support. One could try to simplify the definition of $\PRat$ replacing $\Arith \cup \Geo$ with $\Fin$. Unfortunately, the operators $+, \times, \shift, \shuffle$ are too restricted, and geometric and arithmetic sequences could not be generated.

\begin{lemma}
We have $\Cl[\Fin, +, \times, \shift, \shuffle] = \Fin$.
\end{lemma}

\begin{proof}
Let $\uu^1,\ldots,\uu^k\in\Fin$ et $N$ be a parameter such the sequences are zero for indices larger than $N$.
Then $\uu^1 + \uu^2$ and $\uu^1 \times \uu^2$ are zero for indices larger than N; $\shift(a,\uu^1)$ is zero for indices larger than $N+1$; and $\shuffle(\uu^1,\ldots,\uu^k)$ is zero for indices larger than $Nk$.
Hence $\Cl[\Fin, +, \times, \shift, \shuffle]$ only contains sequences with finite support.
\end{proof}


Since $\Rat$ contains $\Arith$ and $\Geo$ and is closed under Hadamard product, shift, and shuffle,
we have $\PRat \subseteq \Rat$. We will show that the inclusion is indeed strict.
As we will see in this paper, the class $\PRat$ has many equivalent and surprising characterisations.
% Moreover one of the consequences of our results is that $\PRat$ is closed under the Cauchy product\filip{maybe a reference to future corollary?}.


It is also good to notice that the class $\PRat$ is decidable, thanks to the last equivalent characterisation of Theorem~\ref{th:main}.
Indeed, it is enough to decide whether all the roots of a given polynomial with rational coefficients are roots of rational numbers.
For that purpose, we will need the widely used rational root theorem, see~\cite{Redmond89} for instance.

\begin{theorem}[Rational root]
  Let $a_{n}x^{n} + \cdots + a_{0}$ be a polynomial with integer coefficients and $a_{n},a_{0}\neq 0$.
  Every rational root $\frac{p}{q}$ of the polynomial with $p$ and $q$ relatively prime satisfies:
  \begin{itemize}
	\item $p$ divides $a_{0}$,
	\item $q$ divides $a_{n}$.
  \end{itemize}
\end{theorem}

This implies that we can check whether all the roots of a given polynomial with rational coefficients are rational numbers.
We can transform it into an integer polynomial by multiplying it by the gcd of its coefficients, then apply the rational root theorem
to check finitely many candidates for the roots.

\begin{theorem}
  It is decidable whether a sequence is $\PRat$.
\end{theorem}

\begin{proof}
  Let $P = a_{n}x^{n} + \cdots + a_{0}$ be the characteristic polynomial of  the given sequence.
  Notice that if $(\frac{p}{q})^{1/r}$ is a root of $P$, then all its Galois conjugates are too, as $P$ has rational coefficients.
  So $(X^{r}-\frac{p}{q})$ divides $P$.
  It follows that  $r\leq n$.
  Hence the roots of $P$ that are roots of rational numbers must belongs to the finite set $\{ (\frac{p}{q})^{1/r} ~|~r\leq n, ~p\text{ divides  } a_{0}, ~q\text{ divides  }a_{n}\}$.
\end{proof}
