We refer to e.g.~\cite{DrosteHWA09} for an excellent introduction to weighted automata.
We consider weighted automata over the rational semiring $(\Q,+,\cdot)$, where~$+$ and~$\cdot$ are the standard sum and product.
For an alphabet $\Sigma$, weighted automata recognise functions assigning rational numbers to finite words,
\ie $f : \Sigma^* \to \Q$. 
In this paper we will consider only one-letter alphabets so the set of words is $\set{a}^* = \set{\varepsilon, a, a^2, \ldots}$,
which is identified with $\N$.
Therefore, weighted automata recognise functions $f : \N \to \Q$, \ie sequences of rational numbers.

Formally, a weighted automaton is a tuple $\A = (Q, M, I, F)$, where $Q$ is a finite set of states, 
$M$ is a $Q \times Q$ matrix  over $\Q$ and $I, F$ are the initial and final vectors, respectively, of dimension $Q$ (for convenience we label the coordinates by elements of $Q$). 
The sequence recognised by the automaton $\A$ is $\sem{\A}$ defined by $\sem{\A}(n) = I^t M^n F$, 
where $I^t$ is the transpose of $I$. 

We give an equivalent definition of $\A$ in terms of accepting runs. 
We say that a state $q \in Q$ is an initial state if $I(q) \neq 0$ and that it is a final state if $F(q) \neq 0$. If $q$ is initial we say that its initial weight is $I(q)$, and if $q$ is final then its final weight is $F(q)$.
For two states $p,q \in Q$ we say that there is a transition from $p$ to $q$ if $M(p,q) \neq 0$. 
We let $p \to q$ denote such a transition and define its weights as $M(p,q)$.
A run $\rho$ is a sequence of consecutive transitions, and it is accepting if the first state is initial and the last state is final.
The value of an accepting run $\rho = q_0 \to q_1 \to \cdots \to q_n$ is 
\[
|\rho| = I(q_0) \cdot \left( \prod_{i = 0}^{n-1} M(q_i,q_{i+1}) \right) \cdot F(q_n).
\]
Let $\Runs$ denote the set of all accepting runs over $n$. 
An alternative and equivalent definition of $\sem{\A}$ is 
\[
\sem{\A}(n) = \sum_{\rho \in \Runs} |\rho|.
\]

\begin{example}\label{ex:weighted}
Consider the automaton $\A = (Q, M, I, F)$ represented in Figure~\ref{fig:fib}.
We have $\sem{\A}(n) = F_n$, where $\sequence{F}$ is the Fibonacci sequence from Example~\ref{ex:fib}.
\begin{figure}[!ht]
\centering
\includegraphics[scale=.5]{Fig/fibonacci.eps}
\caption{A weighted automaton recognising the Fibonacci sequence.}
\label{fig:fib}
\end{figure}
\end{example}

\begin{example}\label{ex:weighted2}
We fix $k > 0$. Consider the automaton $\B_k = (Q, M, I, F)$ represented in Figure~\ref{fig:binom}.
We have $\sem{\B_k}(n) = \dbinom{n}{k}$. Indeed, given $n$, there are exactly $\dbinom{n}{k}$ accepting runs, corresponding to the choice of which positions in $[1,n]$ go from one state to the next.
\begin{figure}[!ht]
\centering
\includegraphics[scale=.5]{Fig/example2.eps}
\caption{A weighted automaton recognising the binomial coefficient.}
\label{fig:binom}
\end{figure}
\end{example}

The ambiguity of an automaton $\A$ is the function $a_\A : \N \to \N$ which associates to $n$ the number of accepting runs $|\Runs|$.
We consider the following classes:
\begin{itemize}
 \item $\DetWA$ -- the class of deterministic weighted automata, \textit{i.e.} such that there exists at most one $q\in\A$ such that $I(q)\neq 0$, and for any $p\in\A$, there exists at most one $q\in\A$ such that $M(p,q) \neq 0$;
 \item $\kWA$ for fixed $k \in \N$ -- the class of $k$-ambiguous weighted automata, \ie such that $a_\A(n) \le k$ for all $n$;
 \item $\FinWA = \bigcup_{k \in \N}\kWA$ -- the class of finitely ambiguous weighted automata, \ie such that there exists $k$ such that $a_\A(n) \le k$ for all $n$;
 \item $\PolyWA$ -- class of polynomially ambiguous automata, \ie such that there exists a polynomial $P : \N \to \N$ with $a_\A(n) \le P(n)$ for all $n$;
 \item $\WA$ -- the full class of weighted automata.
\end{itemize}

For example, the automaton in Example~\ref{ex:weighted} is not polynomially ambiguous because the number of accepting runs is exponential.
We will see that this is no accident by proving in Section~\ref{sec:lrs} that the Fibonacci sequence is not in $\PolyWA$.

We present our first characterisation of $\PRat$.

\begin{theorem}\label{th:poly}
$\PRat = \PolyWA$
\end{theorem}

\subsection*{Proof of Theorem~\ref{th:poly}}
This subsection is divided into two parts for both inclusions.

\subsection*{$\PRat \subseteq \PolyWA$}

\begin{figure}[!ht]
\centering
\includegraphics[scale=.5]{Fig/arithmetic-geometric.eps}
\caption{The weighted automaton on the left recognises the arithmetic sequence with parameters $(a,b)$ and it is linearly ambiguous.
The weighted automaton on the right recognises the geometric sequence with parameters $a,\lambda$ and it is deterministic.}\label{fig:arithgeo}
\label{fig:arithmetic-geometric}
\end{figure}

% Before we proceed recall that a concatenation of two weighted automata $\A_1 = (Q_1, M_1, I_1, F_1)$ and $\A_2 = (Q_2, M_2, I_2, F_2)$ automata is the automaton with states $Q_1 \cup Q_2$, initial function $I_1$ (it assigns zero to states in $Q_2$), and similarly the final function $F_2$. The transition matrix $M$ is as defined by $M_1$ and $M_2$ in the respective components. We additionally ``merge'' final states in $\A_1$ with initial states in $\A_2$. Formally, for every $p_1 \in Q_1$ and $q_2 \in Q_2$ we define $M(p_1,q_2) = \sum_{q_1 \in Q_1}M(p_1,q_1) \cdot F(q_1) \cdot I(q_2)$.

Figure~\ref{fig:arithgeo} shows how to recognise the arithmetic and the geometric sequences.
It remains to prove that the class $\PolyWA$ is closed under the operators.
The sum, and Hadamard product correspond to: union, concatenation, and product of automata, respectively.  
These standard constructions preserve polynomial ambiguity.
Indeed, the ambiguity function of a union is the sum of the ambiguities; and the ambiguity function of product is the product of the ambiguities.
Below we deal with shift and shuffle operators.

Suppose we have a polynomially ambiguous automaton $\A$ for $\uu$ and we want to construct a new polynomially ambiguous automaton $\A'$ for $\langle a,\uu \rangle$. We start with the case when $a = 0$. Then $\A'$ has the same set of states as $\A$ plus one new state $q_0$, which is the only initial state in $\A'$. All transitions from $\A$ are inherited. There are additionally only outgoing transitions from $q_0$ to all states that are initial in $\A$; the weight of each transition is the initial weight of the corresponding state in $\A$. 
Then $\A'$ recognises $\langle 0,\uu \rangle$ and that $\A'$ is polynomially ambiguous.
For $a \neq 0$ it suffices to add one more state that is both initial and final with initial weight $1$ and final weight $a$.

To deal with shuffle we start with the following preliminary construction. Fix some $k > 0$ and a polynomially ambiguous automaton $\A$ recognising $\uu$. We construct $\A[k]$ recognising $\uu' = \langle \underbrace{u_0, 0,\ldots,0}_{k}, \underbrace{u_1, 0,\ldots,0}_{k}, u_2, \ldots \rangle$, \ie elements $u_i$ are separated by $k-1$ elements with $0$. The idea to construct $\A'$ is that the set of states have an additional component $\set{0,\ldots, k-1}$, and they behave like $\A$ every $k$-th step; in the remaining steps they only wait.
Formally, the set of states of $\A[k]$ is $Q \times \set{0,\ldots,k-1}$, where $Q$ is the set of states of $\A$. The initial (respectively final) states are $(q,0)$ such that $q$ is initial (respectively final) in $\A$ with the same weight. For every transition $p \to q$ in $\A$ there is a transition $(p,0) \to (q,1)$ in $\A[k]$ with the same weight. The remaining transitions are $(q,i) \to (q,(i+1) \mod k)$ with weight $1$, defined for every $i > 0$ and every $q\in Q$. Then $\A[k]$ recognises $\uu'$ and it is polynomially ambiguous.

Let $\A_0,\ldots,\A_{k-1}$ be polynomially ambiguous automata recognising the sequences $\uu_0,\ldots,\uu_{k-1}$.
For every $\A_i$ let $\A_i[k]$ be the automaton constructed above, additionally shifted $i$ times with $0$'s. 
Then $\shuffleop{\uu_0,\ldots,\uu_{k-1}}$ is recognised by the disjoint union of $\A_i[k]$, which is polynomially ambiguous.

\subsection*{$\PolyWA \subseteq \PRat$}

\paragraph*{Formal series}
It will be convenient for reasoning to use formal series, which are a different representation for sequences.
The sequence $\sequence{u}$ induces the formal series $S(x) = \sum_{n \in \N} u_n x^n$, 
with the interpretation that the coefficient of $x^n$ is the value of the $n$-th element in the sequence.
Note that a polynomial represents a sequence with a finite support. 

\begin{example}\label{ex:fib}
A standard example of an LRS is the Fibonacci sequence $\sequence{F}$ defined by the recurrence $F_{n+2} = F_{n+1} + F_n$ and initial values $F_0 = 0, F_1 = 1$. 
Its characteristic polynomial is $p(x) = x^2 - x - 1$, whose roots are $\frac{1+\sqrt{5}}{2}$ and $\frac{1 - \sqrt{5}}{2}$. 
The corresponding formal series is $S(x) = \sum_{n=0}^\infty F_nx^n$. 
Using the definition of $F$ we obtain $S(x) = x + xS(x) + x^2S(x)$ and thus $S(x) = \frac{x}{1 - x - x^2}$.
\end{example}

The first step is to decompose polynomially ambiguous automata using a subclass of weighted automata called \emph{chained loops}.

We say that the states $p_0, p_1, \ldots p_{k-1} \in Q$ form a \emph{loop} if $M(p_i, p_j) \neq 0$ for $j = i+1 \mod k$,
and a \emph{path} from $p_0$ to $p_{k-1}$ if $M(p_i, p_j) \neq 0$ for $j = i+1$, % (in particular $p_{k-1}$ has no successor). 
and in both cases they have length $k$.
We say that a loop $p_0, p_1, \ldots p_{k-1}$ is \emph{simple} if the states do not repeat, and similarly for paths.

An automaton is trimmed if every state occurs in at least one accepting run.

\begin{lemma}\label{lem:poly-simple-loop}
In a trimmed polynomially ambiguous weighted automaton, every state is contained in at most one simple loop.
\end{lemma}

This observation is actually a special case of (the easy implication of) the characterisation for polynomially ambiguous automata~\cite{WeberS91}.

\begin{proof}
Let $\A$ an automaton with ambiguity function $f$.
Let $q$ be a state contained in two simple loops $l_0$ and $l_1$ of respective sizes $n_0$ and $n_1$.
Let $u$ (resp. $v$) a path from (resp. to) an accepting state to (resp. from) $q$
of size $l$ (resp. $m$).
For $k\in\N$, the accepting paths of length $kn_0n_1 + l + m$ contains all $u l_{i_1}^{n_{1-i_1}}\cdots l_{i_k}^{n_{1-i_k}} v$ for $i_j$ being either $0$ or $1$.
All these paths being pairwise different, we have $f(kn_0n_1 + l + m) \geq 2^k$.
\end{proof}

A chained loop is a trimmed automaton with the following properties:
\begin{itemize}
	\item it has a unique initial state $q_0$;
	\item it has a unique final state $q_{k-1}$ with $F(q_{k-1}) = 1$;
	\item there is a unique simple path from $q_0$ to $q_{k-1}$, such that no transitions of this path belongs to any loop;
	\item every state is contained in at most one simple loop.
\end{itemize}
The size of the chained loop is the length of the simple path from $q_0$ to $q_{k-1}$.
Examples are given in Figure~\ref{fig:chained}.
We can remark that for each simple loop, there is at most one ongoing and outgoing transition, and that
they have to share a state.
If we relax the condition that no transitions on the simple path belongs to any loop, we obtain a so-called pre-chained loop.
Notice that in that case, there is still at most one ongoing and outgoing transition, but they do not have to share a state anymore.
We can always transform a pre-chained loops in to a chained loops.

\begin{lemma}\label{lem:pre_chained_to_chained}
Every pre-chained loop $\A$ can be turned into an equivalent chained loop.
\end{lemma}

\begin{proof}
Let $q_0\to q_1 \to \cdots \to q_{k-1}$ be the unique simple path from the initial state of $\A$ to its final state.
Assume that it is not already a chained loop, that is to say that there exists $i$ and $j$ such that there is a loop $q_i\to \cdots \to q_j \to p_1\to\cdots\to p_l$ with the $p_i$ not on the simple path.
We duplicate the path $q_i\to \cdots \to q_{j-1}$ to a path $q_i'\to \cdots \to q_{j-1}'$ and we change the transition $p_l \to q_i$ to $p_l \to q_i'$; and we add a transition $q_{j-1}'\to q_j$.
This new automaton is equivalent to $\A$ since it was the only simple loop going through $q_i,\ldots,q_j$. 
After repeating that operation for every transition on the simple path that belongs to a loop, we end up with a chained loop.
\end{proof}

We define the concatenation of two chained loops $\A_1, \A_2$: this is the chained loop
obtained by constructing the union of the two automata
with the initial state being the initial state of $\A_1$, the final state being the final state of $\A_2$, and
rewiring the output of $\A_1$ to the initial state of $\A_2$, see Figure~\ref{fig:chained}.

\begin{figure}[!ht]
\centering
\includegraphics[scale=.4]{Fig/examples.eps}
\caption{Three example chained loops. The initial and final weights are depicted by ingoing and outgoing edges. The chained loop $\A_1$ recognises the sequence defined by $f_1(2n) = 2 \cdot 3^n$, $f_1(2n+1) = 0$ whose power series is $\frac{2}{1 - 3x^2}$. The chained loop $\A_2$ recognises the sequence $f_2(n) = 5^{n+1}$ whose power series is $\frac{5}{1-5x}$. The chained loop $\A_3$ is the concatenation of $\A_1$ and $\A_2$ and it recognises the sequence $f_3(n) = \sum_{i=1}^n f_1(i-1) \cdot f_2(n-i)$ whose power series is $\frac{10x}{(1-3x^2)(1-5x)}$.}\label{fig:chained}
\end{figure}

\begin{lemma}\label{lem:poly-chained}
Every polynomially ambiguous weighted automaton is equivalent to a union of chained loops.
\end{lemma}

\begin{proof}
Let $\A$ be a trimmed polynomially ambiguous weighted automaton.
We will first transform $\A$ into a union of pre-chained loops, then use Lemma~\ref{lem:pre_chained_to_chained} to conclude.
We say that a sequence $q_0, q_1, \dots, q_{k-1}$ induces a pre-chained loop of size $k$ if
$q_0$ is an initial state, $q_0, \ldots, q_{k-1}$ form a simple path, and $q_{k-1}$ is a final state.
The induced automaton is defined accordingly.

There are finitely many such sequences because $k$ is bounded by the number of states of $\A$.
We claim that $\A$ is equivalent to the union of all pre-chained loops induced by such sequences.
To see this, we exhibit a bijection between the accepting runs of $\A$ and the accepting runs of all the pre-chained loops
which preserves the values of the runs.

To define the bijection we decompose runs as follows.
Consider an accepting run $\rho$ of $\A$, the first and last visited states identify one initial and one final state.
Let $q$ the first state appearing multiple times in $\rho$. 
Thanks to Lemma~\ref{lem:poly-simple-loop}, in between each occurrence of $q$ this is the same run, the simple loop containing $q$.
So $\rho$ can be decomposed as $\rho_b \ell^k \rho_a$, where $\ell$ is the unique simple loop containing $q$.
We further decompose $\rho_a$ as $\ell' \rho_a'$ where $\ell'$ is the largest prefix of $\ell$ that is also a prefix of $\rho_a$.
Repeating this decomposition for $\rho_a'$, we obtain a decomposition of $\rho$ into 
\[
q_0 \cdot \ell_0^{m_0} \ell_0' \cdot q_0' \to q_1 \cdot \ell_1^{m_1}\ell_1' \cdot q_1' \to \dots \to q_{k-1} \cdot \ell_{k-1}^{m_{k-1}}\ell_{k-1}' \cdot q_{k-1}',
\]
where $\ell_i$ is a loop over $q_i$ (we can have $m_i = 0$) and $\ell_i'$ is a prefix of $\ell_i$ and 
$q_0,\ldots, q_{k-1}$ forms a simple path.
Through this correspondence the accepting run $\rho$ maps to an accepting run in exactly one induced pre-chained loop: the one induced by $q_0 \ell_0'q_0',\cdots, q_{k-1} \ell_{k-1}'q_{k-1}'$.
Notice that the map from the possible $q_i,q_i'$ and $\ell'i$ to the induced pre-chained loop is a bijection.
To see that the whole map is a bijection, it is enough to remark that there is a bijection between the accepting runs with a decomposition with a given $q_i,q_i',\ell'i$ and the accepting runs in the associated pre-chained loop (parametrised by the $m_i$s).

\end{proof}

Our aim is to use the decomposition result stated in Lemma~\ref{lem:poly-chained} to prove the inclusion 
$\PolyWA \subseteq \PRat$.

\begin{lemma}\label{lem:chained}\hfill
\begin{itemize}
	\item The formal series induced by a chained loop of size $1$ with a loop is of the form $\frac{\alpha}{1 - \lambda x^{\ell}}$,
where $\alpha = I(q_0)$, $\lambda$ is the product of the weights in the loop and $\ell$ is the length of the loop. 
If there is no loop this reduces to $\alpha$.
	\item Let $S_1,S_2$ be the formal series induced by the chained loops $\A_1$ and $\A_2$,
	then the formal series induced by the concatenation of $\A_1$ and $\A_2$ is $x \cdot S_1 \cdot S_2$. 
	\item Let $S_1,S_2$ be the formal series induced by two automata $\A_1$ and $\A_2$,
	then the formal series induced by the union of $\A_1$ and $\A_2$ is $S_1 + S_2$.
\end{itemize}
\end{lemma}

\begin{proof}
The first and the third item are immediate, we focus on the second. %For convenience let us assume that $\A_1(-1) = 0$.
By definition the concatenation of two chained loops recognises the sequence defined by 
\begin{align}\label{eq:cauchy}
\sem{\A}(n) = \sum_{i = 1}^n \sem{\A_1}(i-1) \cdot \sem{\A_2}(n-i) 
= \sum_{i = 0}^{n-1} \sem{\A_1}(i) \cdot \sem{\A_2}(n-i-1),
\end{align}
since an accepting run in the concatenation is the concatenation of an accepting run in $\A_1$ and an accepting run in $\A_2$. The only issue is that the output state of $\A_1$ was changed into a transition, and to include this step we write $\A_1(i-1)$ instead of $\A_1(i)$.
Hence by Equation~\ref{eq:cauchy} the formal series is indeed the Cauchy product of $S_1$ and $S_2$, shifted by one.
\end{proof}

We are now half-way through the proof of the inclusion $\PolyWA \subseteq \PRat$:
thanks to Lemma~\ref{lem:poly-chained}, we can restrict our attention to unions of chained loops,
and thanks to Lemma~\ref{lem:chained}, we know what are the formal series induced by the sequences computed by such automata.
More specifically, they are obtained from formal series of the form $\frac{\alpha}{1-\lambda x^\ell}$
by taking sums and Cauchy products (with an additional shift).

To prove that $\PRat$ contains such sequences it is tempting to attempt showing that the sequences above are in $\PRat$
and the closure of $\PRat$ under sums and Cauchy products.
Unfortunately, the closure under Cauchy product is not clear (although it will follow from the final result that it indeed holds).
We sidestep this issue by observing that we only need to be able to do Cauchy products of formal series of a special form.
Indeed, the formal series described above are of the form $\frac{P}{Q}$ where $P,Q$ are rational polynomials 
and the roots of $Q$ are roots of rational numbers: this is true of $\frac{\alpha}{1-\lambda x^\ell}$ and is clearly closed under sums and Cauchy products (with the additional shift).

\begin{corollary}\label{cor:cauchy}
Sequences with formal series of the form $\frac{P}{Q}$ where $P,Q$ are rational polynomials 
and the roots of $Q$ are roots of rational numbers are closed under the Cauchy product.
\end{corollary}


Notice that every chained loop can be obtained as concatenations of chained loops of size~$1$. Thus Lemma~\ref{lem:chained} gives a characterisation of formal series corresponding to unions of chained loops: these are sums of products of $\frac{\alpha}{1 - \lambda x^{\ell}}$ and polynomials.
We further simplify this characterisation applying the following lemma.

\begin{lemma}\label{lem:euclidian}
Consider the formal series $\frac{P}{Q}$ where $P,Q$ are rational polynomials and the roots of $Q$ are roots of rational numbers.
Then $\frac{P}{Q}$ can be written as the sum of formal series of the form $\frac{R}{(1 - \lambda x^\ell)^k}$ 
for rational polynomials $R$, rational numbers $\lambda$, and $\ell,k$ natural numbers.
\end{lemma}

\begin{proof}
This is a direct consequence of the fact that $\Q[x]$ is a Euclidean ring.
The exact statement following from this is that 
any product $\prod_{i = 1}^n \frac{R_i}{P_i}$ where the polynomials $P_i$ are mutually prime 
(meaning, for each $i$, the polynomials $P_i$ and $\prod_{j \neq i} P_j$ are coprime)
can be written as a sum of $\frac{Q_i}{P_i}$ for some rational polynomials $Q_i$.

To conclude, we observe that any polynomial whose roots are roots of rational numbers
can be written as a product of mutually prime polynomials of the form $(1 - \lambda x^\ell)^k$.
\end{proof}

By Lemma~\ref{lem:chained} and Lemma~\ref{lem:euclidian} it follows that for every finite union of chained loops its formal series is a sum of $\frac{R}{(1 - \lambda x^\ell)^k}$ for rational polynomials $R$, rational numbers $\lambda$, and $\ell,k$ natural numbers.
Combining this with Lemma~\ref{lem:poly-chained}
we get that the formal series computed by $\PolyWA$ are of the same form.
Thus we have reduced proving the inclusion $\PolyWA \subseteq \PRat$ to
proving that sequences whose formal series are sums of formal series of the form $\frac{R}{(1 - \lambda x^\ell)^k}$ are in $\PRat$. 

Since $\PRat$ is closed under sum, it suffices to consider one such formal series.
Moreover, due to the closure under shifts we can assume that the polynomial $R$ is equal to $1$; as stated in the lemma below.

\begin{lemma}\label{lem:rationa-roots}
The sequence whose formal series is $\frac{1}{(1 - \lambda x^\ell)^k}$ is in $\PRat$.
\end{lemma}

\begin{proof}
We know that
\[
\frac{1}{(1 - \lambda x^\ell)^k} = \sum_{n \in \N} \binom{n + k - 1}{k-1} \lambda^n x^{\ell \cdot n}.
\]
Note that $\binom{n + k - 1}{k-1}$ is a polynomial in $n$ of degree at most $k-1$, 
i.e. $\binom{n + k - 1}{k-1} = \sum_{p = 0}^{k-1} a_p n^p$.
It follows that
\[
\frac{1}{(1 - \lambda x^\ell)^k} = \sum_{p = 0}^{k-1}\ a_p \cdot \sum_{n \in \N} n^p \lambda^n x^{\ell \cdot n}
\]
It is enough to prove that for each $p$ the sequence whose formal series is 
\[
\sum_{n \in \N} a_p n^p \lambda^n x^{\ell \cdot n}
\]
is in $\PRat$. 
Using an arithmetic sequence and Hadamard products we construct $\langle a_p n^p \rangle_{n \in \N}$.
Multiplying it using Hadamard product with the geometric sequence $\langle \lambda^n \rangle_{n \in \N}$
yields $\langle a_p n^p \lambda^n \rangle_{n \in \N}$.
Shuffling the obtained sequence with $\ell - 1$ null sequences yields the desired sequence.
\end{proof}

\subsection{Application: the ambiguity hierarchy of weighted automata}
We show that the natural classes of weighted automata defined by ambiguity can be described using subclasses of rational expressions.

\begin{figure}[!ht]
\centering
\includegraphics[scale=.6]{Fig/hierarchy.eps}
\caption{The strict ambiguous hierarchy of weighted automata.}
\label{fig:hierarchy}
\end{figure}

\begin{lemma}\label{lem:wa-classes}\hfill
\begin{itemize}
	\item[(i)] $\DetWA = \bigcup_{\lambda \in \Q} \shuffle\left(\Cl[\Geo_\lambda, \shift]\right)$; ($\shuffle$ can be used only once)
 	\item[(ii)] $\kWA = \sum_{i=1}^k\Cl[\Geo, \shift, \shuffle]$, (\ie $+$ is used at most $k$ times);
	\item[(iii)] $\FinWA = \Cl[\Geo, +, \shift, \shuffle]$.
\end{itemize}
\end{lemma}

\begin{proof}
We start by proving $\DetWA = \bigcup_{\lambda \in \Q} \shuffle\left(\Cl[\Geo_\lambda, \shift]\right)$.

($\subseteq$) An automaton is a lasso if its states can be partitioned into a path such that the last state on the path is in a loop.
Since the automaton is deterministic it is a lasso. Let $\lambda$ be the value obtained by multiplying all values on the loop, let $\ell$ be the length of the loop and let $m$ be the length of the path. Then it is easy to see that the sequence is obtained by first taking a shuffle of $\ell$ sequences in $\Geo_\lambda$ that are shifted accordingly.

($\supseteq$) We already know that $\Geo_\lambda$ are definable by deterministic weighted automata from Figure~\ref{fig:arithmetic-geometric}. Closure under shift follows from the construction in the proof of $\PRat \subseteq \PolyWA$ because it preserves the property of being deterministic. 
%
To prove the general case suppose we have a sequence definable by $\shuffle(u_1,...,u_k)$ where every sequence $u_i$ is defined as shifts of geometric sequences with a common $\lambda$. Then by generalising the automaton in Figure~\ref{fig:arithmetic-geometric} for every $u_i$
there is a deterministic automaton $\A_i$ with a loop of length $1$ whose value is $\lambda$. The final automaton is a lasso constructed as follows.
\begin{itemize}
 \item The nonloop part will be of length $km$, where $m$ is the maximal number of states among all $\A_i$. The weights are defined in such a way that the values for the first $km$ elements are correct.
 \item The loop will be of length $k$ defined in such a way that all values of the first $km+k$ elements are correct. Then the last transition that closes the loop is defined in such a way that the product on the loop is exactly $\lambda$.
\end{itemize}
It is easy to verify that the construction works because all $u_i$ were defined from geometric sequences with the same $\lambda$.

Proof of $\FinWA = \Cl[\Geo, +, \shift, \shuffle]$.

($\subseteq$) By Lemma~\ref{lem:poly-chained} we know that each automaton in $\FinWA$ is a union of chained loops. It is easy to see that every such chained loop has to be a lasso otherwise it would contradict the assumption that the automaton is finitely ambiguous. 
We apply the construction for every lasso as in the proof of $\DetWA = \bigcup_{\lambda \in \Q} \shuffle\left(\Cl[\Geo_\lambda, \shift]\right)$ 
and use $+$ to deal with the union.

($\supseteq$)
Like in the proof of $\DetWA =\bigcup_{\lambda \in \Q} \shuffle\left(\Cl[\Geo_\lambda, \shift]\right)$ all steps except for $\shuffle$ are easy. Suppose we want to define a sequence $\shuffle(u_1,\dots,u_k)$, where every $u_i$ is definable by an automaton $\A_i \in \FinWA$.
Recall the construction of automata $\A_i[k]$ in the proof of closure under shuffle in $\PRat \subseteq \PolyWA$.
This construction keeps the property of being finitely ambiguous, because each component $\A_i[k]$ of the shuffle automaton are never accepting at the same time.

Proof of $\kWA = \sum_{i=1}^k\Cl[\Geo, \shift, \shuffle]$.

($\subseteq$) It suffices to apply the proof for $\FinWA = \Cl[\Geo, +, \shift, \shuffle]$ with a bit more work. We first write the finitely ambiguous automaton as a union of lassos, with the additional conditions that all loops have the same length and that every lasso has only one accepting state. Now because the ambiguity is bounded by $k$, for each position is the loop, there are at most $k$ lassos that are accepting.
We can therefore group the lassos into at most $k$ sets, such that two lassos in a same group are never accepting in the same position on the loop.
The union of all lassos inside a same group is therefore doable in $\Cl[\Geo, \shift, \shuffle]$.
Then the union of the $k$ groups requires at most $k$ sums.

($\supseteq$) This follows the same steps as the proof of the equality $\FinWA = \Cl[\Geo, +, \shift, \shuffle]$.
First it is easy to see that $\kWA$ contains $\Geo$ and is closed by the $\shift$ operation.
Next, we want to show that $\shuffle(u_1,\dots,u_k)$ is in $\kWA$, where every $u_i$ is definable by an automaton $\A_i \in \kWA$.
Recall once again that an automaton realizing this $\shuffle$ is an union of $\A_i[k]$, each of them having the same ambiguity as $\A_i$ and such that they are never accepting at the same time.
Hence the total ambiguity is the maximum of the ambiguity of the $\A_i[k]$, which is bounded by $k$.

This gives already, with the ($\subseteq$) proof, that $\oneWA = \Cl[\Geo, \shift, \shuffle]$.
To conclude the proof, take a sum of $k$ sequences doable with the means of unambiguous automata.
It is indeed in $\kWA$ as the union of $k$ unambiguous automata is at most $k$-ambiguous.
\end{proof}

We give examples witnessing the strict inclusions $\DetWA \subsetneq \FinWA \subsetneq \PolyWA \subsetneq \WA$
and $\kWA \subsetneq \kplusoneWA$.
\begin{lemma}\label{lem:hierarchy}\hfill
\begin{itemize}
	\item[(i)] $\aa = \shuffleop{\langle 2^n \rangle_{n \in \N}, \langle 1 \rangle_{n \in \N}}$ is in $\oneWA$ but not in $\DetWA$,
	\item[(ii)] $\uu^{(k)}$ defined by $u^{(k)}_n = 1^n + 2^n + \cdots + (k+1)^n$ is in $\kplusoneWA$ but not in $\kWA$,
	\item[(iii)] $\vv$ defined by $v_n = n$ is in $\PolyWA$ but not in $\FinWA$;
	\item[(iv)] Fibonacci is in $\WA$ but not in $\PolyWA$.
\end{itemize}
\end{lemma}

We will give here the proofs of the first three items.
The last item will be proved in Section~\ref{sec:lrs}, it follows from the fact that $\PolyWA = \PRat$ 
is equal to the class of LRS whose eigenvalues are roots of rational numbers. 
As mentioned in Example~\ref{ex:fib} the characteristic polynomial of the Fibonacci sequence is
$x^2-x-1$, so its eigenvalues are not roots of rationals.

\begin{proof}
\hfill
\begin{itemize}
  \item The sequence $\aa$ is recognised by an unambiguous weighted automaton with four states.
  Now, assume that $\aa$ can be recognized by some deterministic weigthed automaton. By Lemma~\ref{lem:wa-classes}.(i), there 
  exists a rational $\lambda$ such that $\aa\in \shuffle\left(\Cl[\Geo_\lambda, \shift]\right)$. Let $n_0$ be the constant of the shift and $p$ be the number of shuffled sequences that are equals to 
  $\aa$. Without loss of generality, we can assume that $p$ and $n_0$ are even. With those parameters, we have that $\aa_{n_0 + p} = \lambda \aa_{n_0}$ and $\aa_{n_0 + p+1} = \lambda \aa_{n_0+1}$. But 
 we know that $\aa_{n_0} = 2^{n_0/2}$ and $\aa_{n_0 + p} = 2^{(n_0+p)/2}$ and $\aa_{n_0+1} = \aa_{n_0+p+1} = 1$. This is a contradiction because $\lambda$ is equal to two distinct values.
 
  \item The sequence $\uu^{(k)}$ is recognised by a $(k+1)$-ambiguous weighted automaton with $k+1$ states.
For the other part, assume $\uu^{(k)}$ is in $\kWA$, which is the same as $\sum_{i=1}^k\Cl[\Geo, \shift, \shuffle]$ (Lemma~\ref{lem:wa-classes}.(ii)). This means that we can find a shift $n_0$ and a period $p$ such that 
  $\uu^{(k)}_{n_0+pn} = a_1 \lambda_1^n + \cdots + a_k \lambda_k^n$ for some constants $\lambda_1 < \cdots < \lambda_k$. Note that if several $\lambda_i$ were equal, then we could factorize to obtain a sum of the same form with less terms, making the argument even easier.
  So for every $n$, 
  $1^{n_0+pn} + \cdots + (k+1)^{n_0+pn} = a_1 \lambda_1^n + \cdots + a_k \lambda_k^n$. The left side is asymptotic equivalent to $(k+1)^{n_0+pn}$ and the right side to $a_k \lambda_k^n$.
  So we obtain $a_k = (k+1)^{n_0}$ and $\lambda_k = (k+1)^p$,
  therefore $a_k \lambda_k^n = (k+1)^{n_0+pn}$. We can thus simplify both sides of the equation and repeat recursively the process, leading to $ 1 =0$, which is a contradiction.
  
  \item First, $\vv$ is an arithmetic sequence so it is in $\PolyWA$. Now assume it is in $\FinWA$. For the same reasons as before, there are a shift $n_0$, a period $p$ and constants 
  $(\lambda_i)_{1\leq i \leq m}$ such that $n_0 +pn = \lambda_1^n + \cdots + \lambda_m^n$. 
  A simple asymptotic argument shows that those two sequences cannot be equal.  
\end{itemize}
\end{proof}

